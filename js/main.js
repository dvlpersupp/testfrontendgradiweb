//CONFIGURACION API
const URLBASE = 'https://api.openweathermap.org';
const API_ID = '14a2412cbc1270be3abbc4d561861477';
const UNITS = 'metric';
const LANG = 'es';

//ID POR CIUDADES
const BOGOTAID = 3688689;
const LYONID = 6454573;
const PARISID = 2988506;

var flagDataCity = 0;
var flagDataForecast = 0;

var fadeOut = (element) => {
  element.style.opacity = 1;
  (function fade() {
      if ((element.style.opacity -= .1) < 0) {
          element.style.display = "none";
      } else {
          requestAnimationFrame(fade);
      }
  })();
}

var removeLoader = () => {
  if(flagDataCity && flagDataForecast) {
    let item = document.getElementById('page-loader');
    fadeOut(item);
  }
}

/**************************************************************
 *                insertar ciudades en el DOM
 *************************************************************/
var insertCity = (dataCity) => {
  //var
  let name = dataCity.name;
  let city = dataCity.id;
  let temp = parseInt(dataCity.main.temp);
  let weather = dataCity.weather[0].main;
  let lat = dataCity.coord.lat;
  let lon = dataCity.coord.lon;
  let icon = dataCity.weather[0].icon;
  let humidity = dataCity.main.humidity;
  let description = dataCity.weather[0].description;

  //crear elemento flotante
  const CreateWeatherFloat = () => {
    let weatherFloat = document.getElementById('weather-float');
    let degreeFloat = document.getElementById('degree-float');

    //elementos para weather float
    let divElementIcon = document.createElement('div');
    divElementIcon.className = 'icon';
    let spanElementWeather = document.createElement('span');
    spanElementWeather.className = 'weather'
    spanElementWeather.innerHTML = weather;
    let imgElement = document.createElement('img');
    imgElement.src = (`${URLBASE}/img/w/${icon}.png`)
    imgElement.alt = description;
    imgElement.title = description;

    //removiendo el loader 
    (weatherFloat.firstElementChild.className == 'loader') ?
      weatherFloat.removeChild(weatherFloat.firstElementChild) : null

    //construyendo elementos
    divElementIcon.appendChild(imgElement);
    weatherFloat.appendChild(divElementIcon);
    weatherFloat.appendChild(spanElementWeather);

    //elementos para degree float
    degreeFloat.innerHTML = `${temp}º`;
  }

  const createCardWeather = () => {
    let cardWeather = document.getElementById('card-weather-2');

    //icon
    let imgElementIcon = document.createElement('img');
        imgElementIcon.src = `${URLBASE}/img/w/${icon}.png`;
        imgElementIcon.alt = description;
        imgElementIcon.title = description;
    let spanElementIcon = document.createElement('span');
        spanElementIcon.className = 'icon';
        spanElementIcon.appendChild(imgElementIcon); 

    //data
      //degree
    let spanElementDegrees = document.createElement('span');
        spanElementDegrees.className = 'degrees';
        spanElementDegrees.innerHTML = `${temp}º`
      //location
    let spanElementCity = document.createElement('span');
        spanElementCity.className = 'city';
        spanElementCity.innerHTML = name;
    let spanElementCountry = document.createElement('span');
        spanElementCountry.className = 'country';
        spanElementCountry.innerHTML = 'Francia';
    let divElementLocation = document.createElement('div');
        divElementLocation.className = 'location';
        divElementLocation.appendChild(spanElementCity);
        divElementLocation.appendChild(spanElementCountry);

    let divElementData = document.createElement('div');
        divElementData.className = 'data';
        divElementData.appendChild(spanElementDegrees);
        divElementData.appendChild(divElementLocation);

      //details
    let spanElement = document.createElement('span');
        spanElement.innerHTML = `Humidity ${humidity}%`;
    let spanElementTwo = document.createElement('span');
        spanElementTwo.innerHTML = (name == 'Lyon') ? `Nortwest` : `West`;
    let spanElementThree = document.createElement('span');
        spanElementThree.innerHTML = `lon: ${lon}, lat: ${lat}`;
    let divElementDetail = document.createElement('div');
        divElementDetail.className = 'details';
        divElementDetail.appendChild(spanElement);
        divElementDetail.appendChild(spanElementTwo);
        divElementDetail.appendChild(spanElementThree);

    let card = document.createElement('div');
        card.className = (name == 'Lyon') ? 'card-weather-2 float' : 'card-weather-2';
        card.appendChild(spanElementIcon);
        card.appendChild(divElementData);
        card.appendChild(divElementDetail);
    
        cardWeather.insertBefore(card, cardWeather.firstElementChild);
  }

  switch (city) {
    case BOGOTAID:
      CreateWeatherFloat();
      break;
    case (LYONID):
      createCardWeather();
      break;
    case (PARISID):
      createCardWeather();
      break;
    default:
      break;
  }
}

/**************************************************************
 *                insertar pronosticos en el DOM
 *************************************************************/
var insertForecast = (dayForecast) => {
  //var
  let weekDays = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday')
  let date = new Date(dayForecast.dt * 1000);
  let dayString = weekDays[date.getDay()];
  let tempMin = parseInt(dayForecast.main.temp_min);
  let tempMax = parseInt(dayForecast.main.temp_max);
  let weather = dayForecast.weather[0].main;
  let icon = dayForecast.weather[0].icon;
  let description = dayForecast.weather[0].description;

  cardsWheather = document.getElementById(`cards-wheather`)

  let divElementItem = document.createElement('div');
  divElementItem.className = 'item days';
  let spanElementIcon = document.createElement('span');
  spanElementIcon.className = 'icon';
  let imgElement = document.createElement('img');
  imgElement.src = `${URLBASE}/img/w/${icon}.png`;
  imgElement.alt = description;
  imgElement.title = description;
  let divElementInfo = document.createElement('div');
  divElementInfo.className = 'info';
  let spanElementDay = document.createElement('span');
  spanElementDay.className = 'day';
  spanElementDay.innerHTML = dayString;
  let spanElementType = document.createElement('span');
  spanElementType.className = 'type-day';
  spanElementType.innerHTML = weather;
  let spanElementgrade = document.createElement('span');
  spanElementgrade.className = 'grade active';
  spanElementgrade.innerHTML = `${tempMin}º / ${tempMax}º`;

  //removiendo el loader
  (cardsWheather.firstElementChild.className == 'loader dark') ?
    cardsWheather.removeChild(cardsWheather.firstElementChild) : null
  //construyendo elementos
  spanElementIcon.appendChild(imgElement);
  divElementInfo.appendChild(spanElementDay);
  divElementInfo.appendChild(spanElementType);
  divElementItem.appendChild(spanElementIcon);
  divElementItem.appendChild(divElementInfo);
  divElementItem.appendChild(spanElementgrade);
  cardsWheather.appendChild(divElementItem);
}

/**************************************************************
 *     PETICIONES AL SERVIDOR DEL PRONOSTICO CLIMATICO
 *************************************************************/

//Solicitud de pronostico por paises/ciudades
http_request_country = new XMLHttpRequest();
http_request_country.onreadystatechange = (() => {

  if (http_request_country.readyState == 4) {
    if (http_request_country.status == 200) {
      //Proceso completado Exitosamente
      let PostArray = JSON.parse(http_request_country.responseText).list;

      PostArray.forEach(city => {
        insertCity(city);
      });

      flagDataCity = 1;
      removeLoader();
    } else {
      console.error(`Error: ${xhr.status}`);
    }
  } else {
    console.log('processing');
  }

});

//Solicitud de pronostico a Bogota
http_request_forecast = new XMLHttpRequest();
http_request_forecast.onreadystatechange = (() => {

  if (http_request_forecast.readyState == 4) {
    if (http_request_forecast.status == 200) {
      //Proceso completado Exitosamente
      let PostArray = JSON.parse(http_request_forecast.responseText).list;

      let listDay = [];
      let dateCurrent = new Date();
      let day = dateCurrent.getDate();

      PostArray.forEach(forecast => {
        let date = new Date(forecast.dt * 1000);
        let dayForescast = date.getDate();

        if (day != dayForescast && listDay.length < 3) {
          listDay.push(forecast);
          day = dayForescast;
        }

      });

      listDay.forEach(dayForecast => {
        insertForecast(dayForecast);
      });

      flagDataForecast = 1;
      removeLoader();
    } else {
      console.error('Error en petición');
    }
  } else {
    console.log('processing');
  }

});


/**************************************************************
 *       Ejecucion de funciones al cargar la pagina
 *************************************************************/
document.addEventListener('DOMContentLoaded', function () {
  //solicitud de clima por ciudades
  http_request_country.open('GET', 
    `${URLBASE}/data/2.5/group?id=${BOGOTAID},${LYONID},${PARISID}&units=${UNITS}&lang=${LANG}&appid=${API_ID}`, true);
  http_request_country.send();

  //solicitud de pronostico para bogota
  http_request_forecast.open('GET',
    `${URLBASE}/data/2.5/forecast?id=${BOGOTAID}&units=${UNITS}&cnt=28&lang=${LANG}&appid=${API_ID}`, true);
  http_request_forecast.send();
});