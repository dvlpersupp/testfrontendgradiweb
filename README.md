# Supervisor del clima Gradiweb 

En Gradiweb estamos comprometidos con nuestros colaboradores, por ello hemos
decidido que algunos días de la semana, realizaremos actividades al aire libre, como
crossfit, 
pícnics entre otros. Sin embargo el clima en Bogotá no nos está ayudando, llueve mucho
por estos días..
Por ello hemos decidido construir una pequeña aplicación web que nos permita conocer el
clima del día actual

## Comenzando 🚀

Puedes **visualizar** la página de ejemplo en el siguiente enlace 
* [Pagina de Ejemplo](https://dvlpersupp.gitlab.io/testfrontendgradiweb/) - Visualiza como se ve el desarrollo



### Pre-requisitos 📋

```
 Conexion a intenet
```

### Instalación 🔧

_Solo descarga en repositorio, ingresa al archivo index.html y podras visualizar los datos del clima actual, y los dias siguientes en la ciudad de Bogotá._


## Modificalo tu mismo

_Para modificar el archivo, debes instalar el modulo node-sass incluido en el package.json, ejecutando 'NPM INSTALL'_

_Luego de haber instalado las dependencia en el archivo scss encontraras las hojas de estilos, en el index.html la estructura de la página y dentro de carpetas referenciadas veras las imágenes y el archivo js encargada del renderizar la información del API_

_Las vistas html y los archivos js, serán actualizadas inmediatamente modifiques, sin embargo el scss necesita ser transpilado. Para iniciar la transpilacion automática basta con ejecutar: 'npm run sass' desde el directorio root del proyecto, esto activara un oyente que estara atento a tus cambios, transpilara y actualizara el css del proyecto_
